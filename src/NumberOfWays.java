import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class NumberOfWays {
    public static void main(String[] args) {

        int n = 0;
        List<Integer> arrayOfCoins = new ArrayList<>();

        System.out.println("Введите сумму продовца N: ");
        Scanner scanner = new Scanner(System.in);
        try {
            n = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("У вас неверный формат ввода, введите, пожалуйста, значения ещё раз");
        }

        System.out.println("Введите значения монет, для того чтобы завершить ввод введите 0: ");
        while (true) {

            try {
                int coin = scanner.nextInt();
                if (coin != 0) {
                    arrayOfCoins.add(coin);
                } else {
                    break;
                }

            } catch (InputMismatchException e) {
                System.out.println("У вас неверный формат ввода, введите, пожалуйста, значения ещё раз");
                break;
            }

        }

        System.out.println("Количество способов: " + numberOfWays(n, arrayOfCoins));
    }

    public static int numberOfWays(int n, List<Integer> arrayOfCoins) {

        int[] ways = new int[n + 1];
        ways[0] = 1;

        for (Integer arrayOfCoin : arrayOfCoins) {
            for (int j = 0; j < ways.length; j++) {

                if (arrayOfCoin <= j)
                    ways[j] += ways[j - arrayOfCoin];

            }
        }

        return ways[n];
    }

}