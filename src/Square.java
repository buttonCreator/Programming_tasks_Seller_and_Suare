import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Square {

    public static void main(String[] args) {

        int n = 0;
        List<List<Integer>> coordinatesList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите количество прямоугольников: ");
        try {
            n = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("У вас неверный формат ввода, введите, пожалуйста, значения ещё раз");
        }

        for (int i = 0; i < n; i++) {
            System.out.print("Введите координаты углов для " + i + 1 + " прямоугольника в формате \"х1, y1, x2, y2\": ");
            List<Integer> coordinates = new ArrayList<>();
            for (int j = 0; j < 4; j++) {
                try {
                    coordinates.add(scanner.nextInt());
                } catch (InputMismatchException e) {
                    System.out.println("У вас неверный формат ввода, введите, пожалуйста, значения ещё раз");
                }
            }
            coordinatesList.add(coordinates);
        }

        System.out.println("Площадь покрытое прямоугольниками: " + squareAll(n, coordinatesList));
    }

    public static int squareAll(int n, List<List<Integer>> coordinatesList) {

        int extraSpaces = 0;
        int result = 0;

        for (int i = 0; i < n - 1; i++) {

            result += squareOne(coordinatesList.get(i));

            for (int j = i + 1; j < n; j++) {

                int extraSpace = crossroads(coordinatesList.get(i), coordinatesList.get(j));
                if (extraSpace != 0)
                    extraSpaces += extraSpace;


            }
        }

        return result - extraSpaces;
    }

    public static int crossroads(List<Integer> rectangleOne, List<Integer> rectangleTwo) {

        int left = Math.max(rectangleOne.get(0), rectangleTwo.get(0));
        int top = Math.max(rectangleOne.get(1), rectangleTwo.get(1));
        int right = Math.max(rectangleOne.get(2), rectangleTwo.get(2));
        int bottom = Math.max(rectangleOne.get(3), rectangleTwo.get(3));

        int width = right - left;
        int height = top - bottom;

        if (width < 0 || height < 0)
            return 0;

        return width * height;
    }

    public static int squareOne(List<Integer> rectangle) {

        int width = Math.abs(rectangle.get(0) - rectangle.get(2));
        int height = Math.abs(rectangle.get(1) - rectangle.get(3));

        return width * height;
    }

}
